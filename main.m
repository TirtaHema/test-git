warning('off', 'all');
pkg load symbolic;

d = 2;
tol = 1e-7;

initial_guess = ones(d, 1);
global_minimum = zeros(d, 1);

f = ackley_function(d);
d_f = gradient(f);
hessian_f = hessian(f);

% initial_time = cputime;
% x_newton = newton(f, d_f, hessian_f, initial_guess, tol);
% end_time = cputime;
% sum_error = sum(x_newton - global_minimum)
% fprintf('Newton, Error: %i, Time: %i\n', sum_error, end_time - initial_time);
% x_newton

% initial_time = cputime;
% x_steepest_descent = steepest_descent(f, d_f, initial_guess, tol);
% end_time = cputime;
% sum_error = sum(x_steepest_descent - global_minimum)
% fprintf('Steepest Descent, Error: %i, Time: %i\n', sum_error, end_time - initial_time);
% x_steepest_descent

% initial_time = cputime;
% x_quasi_newton = quasi_newton(f, d_f, initial_guess, tol);
% end_time = cputime;
% sum_error = sum(x_quasi_newton - global_minimum)
% fprintf('Quasi Newton SR-1, Error: %i, Time: %i\n', sum_error, end_time - initial_time);
% x_quasi_newton